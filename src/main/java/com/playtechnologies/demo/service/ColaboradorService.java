package com.playtechnologies.demo.service;

import com.playtechnologies.demo.model.Colaborador;
import com.playtechnologies.demo.repository.ColaboradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ColaboradorService {
    private final ColaboradorRepository colaboradorRepository;

    @Autowired
    public ColaboradorService(ColaboradorRepository colaboradorRepository) {
        this.colaboradorRepository = colaboradorRepository;
    }

    public Optional<Colaborador> saveColaborador(Colaborador colaborador){
        return Optional.of(colaboradorRepository.save(colaborador));
    }
    public Optional<Colaborador> updateColaborador(Colaborador colaborador){
        return Optional.of(colaboradorRepository.save(colaborador));
    }
    public Optional<Colaborador> findByIdColaborador(Long id){
        return colaboradorRepository.findById(id);
    }
    public List<Colaborador> findAllColaborador(){
        return colaboradorRepository.findAll();
    }
    public void deleteColaborador(Long id){
        colaboradorRepository.deleteById(id);
    }
}
