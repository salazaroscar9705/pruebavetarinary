package com.playtechnologies.demo.service;


import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.repository.CredencialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CredencialService {

    private final CredencialRepository credencialRepository;

    @Autowired
    public CredencialService(CredencialRepository credencialRepository) {
        this.credencialRepository = credencialRepository;
    }

    public Optional<Credencial> saveCredencial(Credencial credencial){
        return Optional.of(credencialRepository.save(credencial));
    }
    public Optional<Credencial> updateCredencial(Credencial credencial){
        return Optional.of(credencialRepository.save(credencial));
    }
    public Optional<Credencial> findByIdCredencial(Long id){
        return credencialRepository.findById(id);
    }

    public List<Credencial> findAllcredencial(){
        return credencialRepository.findAll();
    }

    public void deleteCredencial(Long id){
        credencialRepository.deleteById(id);
    }

    public Optional<Credencial> findByCorreo(String correo) {
        return credencialRepository.findByCorreo(correo);
    }

    public Boolean existsUserByCorreo(String correo){
        return credencialRepository.existsUserByCorreo(correo);
    }
}
