package com.playtechnologies.demo.service;

import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public Optional<Cliente> saveCliente(Cliente cliente){
        return Optional.of(clienteRepository.save(cliente));
    }
    public Optional<Cliente> updateCliente(Cliente cliente){
        return Optional.of(clienteRepository.save(cliente));
    }
    public Optional<Cliente> findByIdCliente(Long id){
        return clienteRepository.findById(id);
    }
    public List<Cliente> findAllCliente(){
        return clienteRepository.findAll();
    }
    public void deleteCliente(Long id){
        clienteRepository.deleteById(id);
    }
}
