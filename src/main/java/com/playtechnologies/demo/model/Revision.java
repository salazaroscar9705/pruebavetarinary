package com.playtechnologies.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "revisiones")
public class Revision {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "hora")
    private LocalTime hora;

    @Column(name = "temperatura")
    private Float temperatura;

    @Column(name = "peso")
    private Float peso;

    @Column(name = "frecuencia_cardiaca")
    private Integer frecuenciaCardiaca;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "estado")
    private Boolean estado;

    @ManyToOne
    @JoinColumn( name = "id_historia_clinica", referencedColumnName = "id")
    private HistoriaClinica historiaClinica;

    @ManyToOne
    @JoinColumn( name = "id_colaborador", referencedColumnName = "id")
    private Colaborador colaborador;


    public Revision() {
        // Constructor
    }

    public Revision(Revision revision) {
        this.id = revision.id;
        this.fecha = revision.fecha;
        this.hora = revision.hora;
        this.temperatura = revision.temperatura;
        this.peso = revision.peso;
        this.frecuenciaCardiaca = revision.frecuenciaCardiaca;
        this.observacion = revision.observacion;
        this.estado = revision.estado;
        this.historiaClinica = revision.historiaClinica;
        this.colaborador = revision.colaborador;
    }

    public Revision(Long id, LocalDate fecha, LocalTime hora, Float temperatura, Float peso, Integer frecuenciaCardiaca, String observacion, Boolean estado, HistoriaClinica historiaClinica, Colaborador colaborador) {
        this.id = id;
        this.fecha = fecha;
        this.hora = hora;
        this.temperatura = temperatura;
        this.peso = peso;
        this.frecuenciaCardiaca = frecuenciaCardiaca;
        this.observacion = observacion;
        this.estado = estado;
        this.historiaClinica = historiaClinica;
        this.colaborador = colaborador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public Float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Integer getFrecuenciaCardiaca() {
        return frecuenciaCardiaca;
    }

    public void setFrecuenciaCardiaca(Integer frecuenciaCardiaca) {
        this.frecuenciaCardiaca = frecuenciaCardiaca;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }
}
