package com.playtechnologies.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RequestExceptionGeneral extends RuntimeException{
    public RequestExceptionGeneral(String mensaje) {
        super(mensaje);
    }
    public RequestExceptionGeneral(String mensaje, Throwable error) {
        super(mensaje, error);
    }
}
