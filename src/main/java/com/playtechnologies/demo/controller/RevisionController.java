package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.exception.RequestExceptionGeneral;
import com.playtechnologies.demo.model.Revision;
import com.playtechnologies.demo.service.RevisionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/revision")
@Api("revision")
public class RevisionController {
    private final RevisionService revisionService;

    @Autowired
    public RevisionController(RevisionService revisionService) {
        this.revisionService = revisionService;
    }
    @PostMapping(path = "/save")
    @ApiOperation(value = "Crear una revisión", response = Revision.class)
    public ResponseEntity<Revision> saveRevision(@RequestBody Revision revision){
        revision.setFecha(LocalDate.now());
        revision.setHora(LocalTime.now());
        Optional<Revision> revisionOptional = revisionService.saveRevision(revision);
        if(revisionOptional.isPresent())
            return new ResponseEntity<>(revisionOptional.get(), HttpStatus.OK);

        else
            return new ResponseEntity(null,HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="listar revisiones",response = Revision.class)
    public ResponseEntity<List<Revision>> findAll(){
        return new ResponseEntity<>(revisionService.findAllRevision(), HttpStatus.OK);

    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "actualizar revision",response = Revision.class)
    public ResponseEntity<Revision> updateRevision(@RequestBody Revision revision){
        return new ResponseEntity<>(revisionService.updateRevision(revision).get(), HttpStatus.OK);
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "revision por id", response = Revision.class)
    public ResponseEntity<Revision> findById(@RequestParam(name = "id") Long id){
        return ResponseEntity.ok(revisionService.findByIdRevision(id)
                .orElseThrow(() -> new RequestExceptionGeneral("No se encontró la revisión ")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "eliminar por id",response = Revision.class)
    public void deleteById(@RequestParam(name="id") Long id){
        revisionService.deleteRevision(id);
    }
}
