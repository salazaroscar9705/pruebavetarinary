package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.exception.RequestExceptionGeneral;
import com.playtechnologies.demo.model.HistoriaClinica;
import com.playtechnologies.demo.service.HistoriaClinicaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/historiaClinica")
@Api("historiaClinica")
public class HistoriaClinicaController {

    private final HistoriaClinicaService historiaClinicaService;

    @Autowired
    public HistoriaClinicaController(HistoriaClinicaService historiaClinicaService) {
        this.historiaClinicaService = historiaClinicaService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Crear una historia clinica", response = HistoriaClinica.class)
    public ResponseEntity<HistoriaClinica> saveHistoriaClinica(@RequestBody HistoriaClinica historiaClinica){
        historiaClinica.setFecha(LocalDate.now());
        historiaClinica.setHora(LocalTime.now());
        Optional<HistoriaClinica> historiaClinicaOptional = historiaClinicaService.saveHistoriaClinica(historiaClinica);
        if(historiaClinicaOptional.isPresent())
            return new ResponseEntity<>(historiaClinicaOptional.get(), HttpStatus.OK);

        else
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="listar historias clinicas",response = HistoriaClinica.class)
    public ResponseEntity<List<HistoriaClinica>> findAll(){

        return new ResponseEntity<>(historiaClinicaService.findAllHistoriaClinica(), HttpStatus.OK);

    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "actualizar historia clinica",response = HistoriaClinica.class)
    public ResponseEntity<HistoriaClinica> updateHistoriaClinica(@RequestBody HistoriaClinica historiaClinica){
        return new ResponseEntity<>(historiaClinicaService.updateHistoriaClinica(historiaClinica).get(), HttpStatus.OK);
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Historia Clinica por id", response = HistoriaClinica.class)
    public ResponseEntity<HistoriaClinica> findById(@RequestParam(name = "id") Long id){
        return ResponseEntity.ok(historiaClinicaService.findByIdHistoriaClinica(id)
                .orElseThrow(() -> new RequestExceptionGeneral("No se encontró la historia clinica")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "eliminar por id",response = HistoriaClinica.class)
    public void deleteById(@RequestParam(name="id") Long id){
        historiaClinicaService.deleteHistoriaClinica(id);
    }
}
