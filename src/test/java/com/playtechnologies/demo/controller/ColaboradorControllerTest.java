package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.factory.ColaboradorFactory;
import com.playtechnologies.demo.factory.CredencialFactory;
import com.playtechnologies.demo.model.Colaborador;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.service.ColaboradorService;
import com.playtechnologies.demo.service.CredencialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ColaboradorControllerTest {
    private Colaborador colaborador;
    private Optional<Colaborador> colaboradorOptional;
    private Credencial credencial;
    private Optional<Credencial> credencialOptional;

    @Mock
    private ColaboradorService colaboradorService;

    @Mock
    private CredencialService credencialService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private ColaboradorController colaboradorController;

    @BeforeEach
    public void init(){
        colaborador = new ColaboradorFactory().crearEntidad();
        colaboradorOptional = Optional.of(colaborador);
        credencial = new CredencialFactory().crearEntidad();
        credencialOptional = Optional.of(credencial);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveColaboradorTest(){
        Mockito.when(colaboradorService.saveColaborador(colaborador)).thenReturn(colaboradorOptional);
        Mockito.when(credencialService.saveCredencial(credencial)).thenReturn(credencialOptional);
        Mockito.when(passwordEncoder.encode(colaborador.getContrasenia())).thenReturn(colaborador.getContrasenia());

        ResponseEntity<Colaborador> colaborador1 = colaboradorController.saveColaborador(colaborador);

        assertEquals(colaborador1.getStatusCodeValue(), 200);
    }

    @Test
    void updateColaboradorTest(){
        Mockito.when(colaboradorService.updateColaborador(colaborador)).thenReturn(colaboradorOptional);

        ResponseEntity<Colaborador> colaborador1 = colaboradorController.updateColaborador(colaborador);

        assertEquals(colaborador1.getStatusCodeValue(), 200);
    }

    @Test
    void findByIdTest(){
        Mockito.when(colaboradorService.findByIdColaborador(colaborador.getId())).thenReturn(colaboradorOptional);

        ResponseEntity<Colaborador> colaborador1 = colaboradorController.findById(colaborador.getId());

        assertNotNull(colaborador1);
        assertEquals(colaborador1.getStatusCodeValue(),200);
        assertEquals(colaborador1.getBody(), colaborador);

    }

    @Test
    void findByAllTest(){
        final Colaborador colaborador = new Colaborador();

        Mockito.when(colaboradorService.findAllColaborador()).thenReturn(Arrays.asList(colaborador));

        final ResponseEntity<List<Colaborador>> response = colaboradorController.findAll();

        assertEquals(response.getStatusCodeValue(),200);
        assertEquals(response.getBody().size(),1);
    }

    @Test
    void deleteByIdTest() {
        colaboradorController.deleteById(colaborador.getId());
        Mockito.verify(colaboradorService, Mockito.times(1)).deleteColaborador(colaborador.getId());
    }


}
