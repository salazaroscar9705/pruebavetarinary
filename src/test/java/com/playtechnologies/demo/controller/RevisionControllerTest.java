package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.factory.RevisionFactory;
import com.playtechnologies.demo.model.Revision;
import com.playtechnologies.demo.service.RevisionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RevisionControllerTest {
    private Revision revision;
    private Optional<Revision> revisionOptional;

    @Mock
    private RevisionService revisionService;

    @InjectMocks
    private RevisionController revisionController;

    @BeforeEach
    public void init(){
        revision = new RevisionFactory().crearEntidad();
        revisionOptional = Optional.of(revision);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveRevisionTest(){
        Mockito.when(revisionService.saveRevision(revision)).thenReturn(revisionOptional);

        ResponseEntity<Revision> revision1 = revisionController.saveRevision(revision);

        assertEquals(revision1.getStatusCodeValue(), 200);
    }
    @Test
    void updateRevisionTest(){
        Mockito.when(revisionService.updateRevision(revision)).thenReturn(revisionOptional);

        ResponseEntity<Revision> revision1 = revisionController.updateRevision(revision);

        assertEquals(revision1.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void findByIdTest(){
        Mockito.when(revisionService.findByIdRevision(revision.getId())).thenReturn(revisionOptional);

        ResponseEntity<Revision> revision1 = revisionController.findById(revision.getId());

        assertNotNull(revision1);
        assertEquals(revision1.getStatusCodeValue(),200);
        assertEquals(revision1.getBody(), revision);

    }

    @Test
    void findByAllTest(){
        final Revision revision = new Revision();

        Mockito.when(revisionService.findAllRevision()).thenReturn(Arrays.asList(revision));

        final ResponseEntity<List<Revision>> response = revisionController.findAll();

        assertEquals(response.getStatusCodeValue(),200);
        assertEquals(response.getBody().size(),1);
    }

    @Test
    void deleteByIdTest() {
        revisionController.deleteById(revision.getId());
        Mockito.verify(revisionService, Mockito.times(1)).deleteRevision(revision.getId());
    }

}
