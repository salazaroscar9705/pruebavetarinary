package com.playtechnologies.demo.service;

import com.playtechnologies.demo.factory.ColaboradorFactory;
import com.playtechnologies.demo.model.Colaborador;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.repository.ColaboradorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ColaboradorServiceTest {

    private Colaborador colaborador;
    private Optional<Colaborador> colaboradorOptional;


    @Mock
    ColaboradorRepository colaboradorRepository;

    @InjectMocks
    ColaboradorService colaboradorService;

    @BeforeEach
    public void init(){
        colaborador = new ColaboradorFactory().crearEntidad();
        colaboradorOptional = Optional.of(colaborador);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveColaboradorTest() {
        Mockito.when(colaboradorRepository.save(colaborador)).thenReturn(colaborador);
        Optional<Colaborador> colaboradorSave = colaboradorService.saveColaborador(colaborador);

        assertEquals(colaboradorSave,Optional.of(colaborador));
    }

    @Test
    void findByIdExitosoTest() {
        Mockito.when(colaboradorRepository.findById(colaborador.getId())).thenReturn(colaboradorOptional);

        Optional<Colaborador> colaborador1 = colaboradorService.findByIdColaborador(colaborador.getId());
        assertEquals(colaborador1, colaboradorOptional);
    }
    @Test
    void findByIdFallidoTest() {
        Mockito.when(colaboradorRepository.findById(colaborador.getId())).thenReturn(null);

        Optional<Colaborador> colaborador1 = colaboradorService.findByIdColaborador(colaborador.getId());
        assertNull(colaborador1);
    }
    @Test
    void updateColaboradorTest() {
        Mockito.when(colaboradorRepository.save(colaborador)).thenReturn(colaborador);

        Optional<Colaborador> colaboradorUpdate = colaboradorService.updateColaborador(colaborador);

        assertNotNull(colaboradorUpdate);
        assertEquals(colaboradorUpdate,Optional.of(colaborador));
    }

    @Test
    void findAllTest() {
        final Colaborador colaborador = new Colaborador();

        Mockito.when(colaboradorRepository.findAll()).thenReturn(Arrays.asList(colaborador));

        final List<Colaborador> list = colaboradorService.findAllColaborador();

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
    }
    @Test
    void deleteColaboradorTest() {

        colaboradorService.deleteColaborador(colaborador.getId());
        Mockito.verify(colaboradorRepository, Mockito.times(1)).deleteById(colaborador.getId());


    }
}
