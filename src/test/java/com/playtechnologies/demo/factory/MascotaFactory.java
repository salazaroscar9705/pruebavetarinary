package com.playtechnologies.demo.factory;

import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Mascota;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;
import java.util.UUID;

public class MascotaFactory {
    private Long id;
    private String nombre;
    private String raza;
    private String sexo;
    private Integer edad;
    private LocalDate fechaCreacion;
    private Boolean estado;
    private Cliente cliente;

    public MascotaFactory() {
        Random random = new Random();
        LocalDate f1 =  LocalDate.of(random.nextInt(40)+2021,random.nextInt(12)+1, random.nextInt(28)+1);
        id = random.nextLong();
        nombre = UUID.randomUUID().toString();
        raza = UUID.randomUUID().toString();
        sexo = UUID.randomUUID().toString();
        edad = random.nextInt();
        fechaCreacion = f1;
        estado = true;
        cliente = new ClienteFactory().crearEntidad();
    }
    public Mascota crearEntidad(){
        return new Mascota(id, nombre, raza, sexo, edad, fechaCreacion,estado, cliente);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
